var mysql = require("mysql");
var client = require('https');
var restify = require('restify')
var server = restify.createServer()

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

var books = require('./books.js');

var dboptions = {
  host     : process.env.IP,
  user     : 'slgarrett9',
  password : '',
  database : 'c9'
};

function googleBookAPI(isbn, callback) {
  var result = "";
  var apikey = "AIzaSyAwMD0dmn0E9lNnJddwkRrO9wG8VHSTbKY";
  
  var options = {
    host: 'www.googleapis.com',
    port:443,
    path: '/books/v1/volumes?q=isbn:' + isbn + '&key=' + apikey,
    method: 'GET'
  };
    
  var req = client.request(options, function(res) {
    res.on('data', function (chunk) {
      result += chunk;
    });
      
    res.on('end', function() {
      callback(result);
    });
  });
  
  req.end();
  
}


server.opts('/\.*/', function(req,res, next) {
   res.header('Access-Control-Allow-Origin','*');
   res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,OPTIONS');
   res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization,Content-Length,X-Requested-With');
   res.send(200);
   return next();
});


// READ BOOK
server.get('/book/:isbn', function (req, res, next) {
  try {
    var connection = mysql.createConnection(dboptions);
    connection.connect();
    var sql = "SELECT * FROM book";
    var params = [];
    if (req.params.isbn) {
      sql += " WHERE isbn = ?";
      params = [req.params.isbn];
    }
    connection.query(sql,params, function(err,result) {
      if (err) res.send(400,{status:'failure',error:err.message});
      else res.send(200,result);
      
    });
    connection.end();
        
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
    
  } finally {
    return next();  
    
  } 
      
 });
 
//SEARCH FOR BOOK
server.get('/book/search/:isbn', function (req, res, next) {
  try {
    googleBookAPI(req.params.isbn,function(data) {
      var results = JSON.parse(data);
      console.log(results);
      res.send(200, results.items);  
    });
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
    
  } finally {
    return next();  
    
  } 
  
});

//UPDATE BOOK
server.put('/book/:id', function (req, res, next) {
  try {

    var connection = mysql.createConnection(dboptions);
    connection.connect();
    connection.query('UPDATE book SET ? WHERE id = ?',[req.body,req.params.id], function(err, result) {
      if (err) res.send(400,{status:'failure',error:err.message});
      else res.send(202,{status:'successful',affectedRows:result.affectedRows});
    });
    connection.end();
      
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
  }

});

//CREATE BOOK
server.post('/book', function (req, res, next) {
  try {
    var connection = mysql.createConnection(dboptions);
    connection.connect();
    connection.query('INSERT INTO book SET ?',[req.body], function(err,result) {
      if (err) res.send(400,{status:'failure',error:err.message});
      else res.send(201,{status:'successful',id:result.insertId});
    });
    connection.end();
      
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
  }

});

//CREATE ORDER AND RETURN NEW UNIQUE ID
server.post('/order', function (req, res, next) {
  try {
    var connection = mysql.createConnection(dboptions);

    //CREATE ORDER RECORD
    connection.query('INSERT INTO book_orders (userid,items) VALUES (?,?)',[req.body.userid, JSON.stringify(req.body.items)], function(err,result) {
      if (err) res.send(400,{status:'failure',error:err.message});
      else {
        req.body.items.forEach(function(item){
          //UPDATE BOOK TABLE
          connection.query('UPDATE book SET quantity = quantity - ? WHERE isbn = ?',[item.qty,item.isbn],function(err,result) {
            if (err) console.log(err.message);
          });
        });
        res.send(201,{status:'successful',id:result.insertId});
      }
    });

      
  } catch(e) {

    res.send(400,{status:'fail',error:e.message});
  }

});

//GET ORDERS
server.get('/myorders/:id', function (req, res, next) {
  try {
    var connection = mysql.createConnection(dboptions);
    connection.connect();
    var sql = "SELECT * FROM book_orders WHERE userid = ?";
    var params = [req.params.id];
        
    connection.query(sql,params, function(err,result) {
      if (err) res.send(400,{status:'failure',error:err.message});
      else {
        var results = [];
        //MY ORDERS
        result.forEach(function(row){
          var items = JSON.parse(row.items);
          var total_price = 0;
          var total_items = 0;
          
          items.forEach(function(item){
            total_price += Number(item.total_price);
            total_items += Number(item.qty);
          });
         
          row.items = items;
          row.total_price = total_price;
          row.total_items = total_items;
          results.push(row);
        });
        res.send(200,results);
      }
    });
    connection.end();
        
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
    
  } finally {
    return next();  
    
  } 
  
 });

//CREATE ACCOUNT
server.post('/account', function (req, res, next) {
  try {
    var connection = mysql.createConnection(dboptions);
    connection.connect();
    connection.query('INSERT INTO account SET ?',[req.body], function(err,result) {
      if (err) res.send(400,{status:'failure',error:err.message});
      else res.send(201,{status:'successful',id:result.insertId});
    });
    connection.end();
      
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
  }

});

//LOGIN AND RETURN UNIQUE CUSTOMER ID
server.put('/account/login', function (req, res, next) {
  try {
    var connection = mysql.createConnection(dboptions);
    connection.connect();
    var sql = "SELECT * FROM account WHERE username = ? AND password = ?";
    var params = [req.body.username, req.body.password];
        
    connection.query(sql,params, function(err,result) {
      if (err) res.send(400,{status:'Fail',error:err.message});
      else if (result.length == 1) res.send(200,{status:"Success",userid:result[0].id});
      else res.send(200,{status:"failure",error:"Invalid username/password or account doesn't exist"});
    });
    connection.end();
        
  } catch(e) {

    res.send(400,{status:'failure',error:e.message});
    
  } finally {
    return next();  
    
  } 
      
 });

var port = process.env.PORT || 8080;
server.listen(port, function (err) {
  if (err) {
      console.error(err);
  } else {
    console.log('App is ready at : ' + port);
  }
})
